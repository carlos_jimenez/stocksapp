//
//  AppDelegate.swift
//  StocksApp
//
//  Created by Carlos Jimenez on 19/6/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        guard let mainWindow = window else {
            return false
        }
        let favoriteListVC = FavoriteListViewController()
        let navController = UINavigationController(rootViewController: favoriteListVC)
        mainWindow.rootViewController = navController
        mainWindow.makeKeyAndVisible()
        return true
    }
}

