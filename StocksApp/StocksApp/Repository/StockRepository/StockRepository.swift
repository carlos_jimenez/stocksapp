//
//  StockRepository.swift
//  StocksApp
//
//  Created by Carlos Jimenez on 19/6/21.
//

import Foundation


protocol StockRepository {
    func fetchFavoriteStocks(_ completionBlock: @escaping (_ favorites: [Stock]) -> Void)
    func deleteStockFromFavorite(stock: Stock, completionBlock: @escaping (_ success: Bool) -> Void)
}
