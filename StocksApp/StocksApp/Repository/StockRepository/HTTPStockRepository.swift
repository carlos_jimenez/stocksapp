//
//  HTTPStockRepository.swift
//  StocksApp
//
//  Created by Carlos Jimenez on 19/6/21.
//

import Foundation
import SwiftCoroutine

class  HTTPStockRepository: StockRepository {
    var favoriteStocksCache: [Stock] = []
    var deletedStocks: [Stock] = []
    
    init() {

    }
    
    func fetchFavoriteStocks(_ completionBlock: @escaping ([Stock]) -> Void) {
        if let url = URL(string: HTTPConstant.baseURL + HTTPConstant.endPoint.favorites) {
           var urlRequest = URLRequest(url: url)
           urlRequest.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
           URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in
                if error != nil {
                    completionBlock(self?.favoriteStocksCache ?? [])
                }
                else if let data = data {
                    let text = String(data: data, encoding: .isoLatin1)
                    NSLog("Response = %@", text ?? "")
                    let jsonDecoder = JSONDecoder()
                    do {
                        let listIds = try jsonDecoder.decode(FavoriteListResult.self, from: data)
                        var stocks: [Stock] = []
                        let dispatchGroup = DispatchGroup()
                        for id in listIds.result {
                            let isDeletedFound = self?.deletedStocks.first(where: { aStock in return id == aStock.id })
                            if  isDeletedFound == nil {
                                dispatchGroup.enter()
                                DispatchQueue.global().async { [weak self] in
                                    self?.getStockDetailWithId(id) { (aStock: Stock) in
                                        stocks.append(aStock)
                                        dispatchGroup.leave()
                                    }
                                }
                            }
                        }
                        dispatchGroup.notify(queue: .main) { [weak self] in
                            self?.favoriteStocksCache = stocks
                            completionBlock(stocks)
                        }
                        
                    }
                    catch {
                        print(error)
                    }
               }
           }.resume()
        }
        
    }
    
    func getStockDetailWithId(_ id: String, completionBlock: @escaping (Stock) -> Void) {
        if let url = URL(string: HTTPConstant.baseURL + HTTPConstant.endPoint.stockDetail + id) {
           URLSession.shared.dataTask(with: url) { data, response, error in
                if let data = data {
                    let text = String(data: data, encoding: .isoLatin1)
                    NSLog("Response = %@", text ?? "")
                    let jsonDecoder = JSONDecoder()
                    do {
                        var stock = try jsonDecoder.decode(Stock.self, from: data)
                        stock.id = id
                        completionBlock(stock)
                    }
                    catch {
                        print(error)
                    }
               }
           }.resume()
        }
    }
    
    
    func  deleteStockFromFavorite(stock: Stock, completionBlock: @escaping (Bool) -> Void) {
        deletedStocks.append(stock)
        completionBlock(true)
    }
    
    
}
