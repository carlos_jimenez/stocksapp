//
//  StockDetailViewModel.swift
//  StocksApp
//
//  Created by Carlos Jimenez on 21/6/21.
//

import Foundation
import UIKit

enum DetailRow {
    case name
    case hot
    case ricCode
    case category
}

protocol StockDetailViewModelDelegate: AnyObject {
    func reloadTable()
    func updateTittle(_ title: String)
}

class StockDetailViewModel {
    private weak var delegate: StockDetailViewModelDelegate?
    private var stock: Stock!
    private let rows: [DetailRow] = [.name, .hot, .ricCode, .category]
    
    var title: String {
        NSLocalizedString("Detail", comment: "")
    }
    
    init(stock: Stock, delegate: StockDetailViewModelDelegate?) {
        self.stock = stock
        self.delegate = delegate
    }
    
    func actionViewDidLoad() {
        
    }
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return 4
    }
    
    func nameStockPropertyAtIndexPath(_ indexPath: IndexPath) -> String {
        let row = rows[indexPath.row]
        switch row {
        case .name:
            return  NSLocalizedString("name", comment: "").uppercased()
        case .hot:
            return NSLocalizedString("hot", comment: "").uppercased()
        case .ricCode:
            return NSLocalizedString("ricCode", comment: "").uppercased()
        case .category:
            return NSLocalizedString("category", comment: "").uppercased()
        }
    }
    
    func valueStockPropertyAtIndexPath(_ indexPath: IndexPath) -> String {
        let row = rows[indexPath.row]
        switch row {
        case .name:
            return stock.name ?? ""
        case .hot:
            var hotValue = NSLocalizedString("no", comment: "")
            (stock.hot == 1) ? (hotValue = NSLocalizedString("yes", comment: "")) : (hotValue = NSLocalizedString("no", comment: ""))
            return hotValue
        case .ricCode:
            return stock.ricCode
        case .category:
            return stock.category
        }
    }
}
