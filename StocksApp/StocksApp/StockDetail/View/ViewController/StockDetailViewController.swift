//
//  StockDetailViewController.swift
//  StocksApp
//
//  Created by Carlos Jimenez on 21/6/21.
//

import UIKit

class StockDetailViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    private var viewModel: StockDetailViewModel!
    
    convenience init(model: Stock) {
        self.init()
        self.viewModel = StockDetailViewModel(stock: model, delegate: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.actionViewDidLoad()
        title = viewModel.title
    }
}

extension StockDetailViewController: StockDetailViewModelDelegate {
    func reloadTable() {
        tableView.reloadData()
    }
    
    func updateTittle(_ title: String) {
        self.title = title
    }
}


extension StockDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = StockDetailTableViewCell.dequeueReusableWithTableView(tableView) as! StockDetailTableViewCell
        cell.nameLabel.text = viewModel.nameStockPropertyAtIndexPath(indexPath)
        cell.valueLabel.text = viewModel.valueStockPropertyAtIndexPath(indexPath)
        return cell
    }
}
