//
//  StockDetailTableViewCell.swift
//  StocksApp
//
//  Created by Carlos Jimenez on 21/6/21.
//

import UIKit

class StockDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    class func dequeueReusableWithTableView(_ tableView: UITableView) -> UITableViewCell {
        var cell: StockDetailTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "StockDetailTableViewCell") as? StockDetailTableViewCell
        if cell == nil {
            tableView.register(UINib(nibName: "StockDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "StockDetailTableViewCell")
            cell = tableView.dequeueReusableCell(withIdentifier: "StockDetailTableViewCell") as? StockDetailTableViewCell
        }
        return cell
    }
    
}
