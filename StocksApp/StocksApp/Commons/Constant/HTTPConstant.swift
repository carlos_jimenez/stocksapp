//
//  HTTPConstant.swift
//  StocksApp
//
//  Created by Carlos Jimenez on 20/6/21.
//

import Foundation

struct HTTPConstant {
    static let baseURL = "https://challenge.ninetynine.com/"
    
    struct endPoint {
        static let favorites = "favorites"
        static let stockDetail = "favorites/"
    }
}
