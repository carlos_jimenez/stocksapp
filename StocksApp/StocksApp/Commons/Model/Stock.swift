//
//  Stock.swift
//  StocksApp
//
//  Created by Carlos Jimenez on 19/6/21.
//

import Foundation

struct Stock: Codable {
    var id: String?
    var name: String?
    var hot: Int
    var ricCode: String
    var category: String
    
    init(id: String, name: String, hot: Int, ricCode: String, category: String) {
        self.id = id
        self.name = name
        self.hot = hot
        self.ricCode = ricCode
        self.category = category
    }
}
