//
//  FavoriteListResult.swift
//  StocksApp
//
//  Created by Carlos Jimenez on 19/6/21.
//

import Foundation

struct FavoriteListResult: Codable {
    var result: [String]
}
