//
//  FavoriteTableViewCell.swift
//  StocksApp
//
//  Created by Carlos Jimenez on 19/6/21.
//

import UIKit

protocol FavoriteTableViewCellDelegate: AnyObject {
    func favoriteTableViewCellDidPressDelete(cell: FavoriteTableViewCell)
}

class FavoriteTableViewCell: UITableViewCell {
    @IBOutlet weak var  nameLabel: UILabel!
    @IBOutlet weak var  hotLabel: UILabel!
    weak var delegate: FavoriteTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    class func dequeueReusableWithTableView(_ tableView: UITableView) -> UITableViewCell {
        var cell: FavoriteTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "FavoriteTableViewCell") as? FavoriteTableViewCell
        if cell == nil {
            tableView.register(UINib(nibName: "FavoriteTableViewCell", bundle: nil), forCellReuseIdentifier: "FavoriteTableViewCell")
            cell = tableView.dequeueReusableCell(withIdentifier: "FavoriteTableViewCell") as? FavoriteTableViewCell
        }
        return cell
    }
    
    @IBAction func deleteButtonPressed() {
        delegate?.favoriteTableViewCellDidPressDelete(cell: self)
    }
    
}
