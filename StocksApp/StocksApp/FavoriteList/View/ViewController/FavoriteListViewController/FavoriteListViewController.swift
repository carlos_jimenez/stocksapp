//
//  FavoriteListViewController.swift
//  StocksApp
//
//  Created by Carlos Jimenez on 19/6/21.
//

import UIKit

class FavoriteListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    private var viewModel: FavoriteListViewModel!
    private var refreshControl: UIRefreshControl = UIRefreshControl()

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.actionViewDidAppear()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let httpRepository = HTTPStockRepository()
        viewModel = FavoriteListViewModel(repository: httpRepository, delegate: self)
        viewModel.actionViewDidLoad()
        title = viewModel.title
        configureRefreshControl()
    }
    
    private func configureRefreshControl() {
        refreshControl.addTarget(self, action: #selector(refreshControlPulled), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    @objc internal func refreshControlPulled() {
        viewModel.actionRefreshControlPulled()
    }
}

extension FavoriteListViewController: FavoriteListViewModelDelegate {
    func updateTittle(_ title: String) {
        self.title = title
    }
    
    func reloadTable() {
        tableView.reloadData()
    }
    
    func deselectRowAtIndexPath(_ indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func deleteRowAtIndexPath(_ indexPath: IndexPath) {
        tableView.beginUpdates()
        tableView.deleteRows(at: [indexPath], with: .automatic)
        tableView.endUpdates()
    }
    
    func endRefreshControl() {
        refreshControl.endRefreshing()
    }
    
    func routeToStockDetailScreenWithModel(_ model: Stock) {
        let vc = StockDetailViewController(model: model)
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension FavoriteListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = FavoriteTableViewCell.dequeueReusableWithTableView(tableView) as! FavoriteTableViewCell
        cell.delegate = self
        cell.nameLabel.text = viewModel.stockNameAtIndexPath(indexPath)
        cell.hotLabel.isHidden = viewModel.isHideHotAtIndexPath(indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.actionDidSelectRowAtIndexPath(indexPath)
    }
}

extension FavoriteListViewController: FavoriteTableViewCellDelegate {
    func favoriteTableViewCellDidPressDelete(cell: FavoriteTableViewCell) {
        if let indexPath = tableView.indexPath(for: cell) {
            viewModel.actionDidSelectDeleteAtIndexPath(indexPath)
        }
    }
}
