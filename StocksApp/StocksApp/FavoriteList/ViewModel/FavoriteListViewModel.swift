//
//  FavoriteListViewModel.swift
//  StocksApp
//
//  Created by Carlos Jimenez on 19/6/21.
//

import Foundation
import UIKit


protocol FavoriteListViewModelDelegate: AnyObject {
    func reloadTable()
    func updateTittle(_ title: String)
    func deselectRowAtIndexPath(_ indexPath: IndexPath)
    func deleteRowAtIndexPath(_ indexPath: IndexPath)
    func endRefreshControl()
    func routeToStockDetailScreenWithModel(_ model: Stock)
}

class FavoriteListViewModel {
    private weak var delegate: FavoriteListViewModelDelegate?
    private var repository: StockRepository?
    private var favoriteStocks: [Stock] = []
    private var indexPathSelected: IndexPath?
    
    var title: String {
        NSLocalizedString("Favorites", comment: "")
    }
    
    init(repository: StockRepository, delegate: FavoriteListViewModelDelegate?) {
        self.repository = repository
        self.delegate = delegate
    }
    
    func actionViewDidAppear() {
        if let indexToDeselect = indexPathSelected {
            delegate?.deselectRowAtIndexPath(indexToDeselect)
            indexPathSelected = nil
        }
    }
    
    func actionViewDidLoad() {
        fetchFavoriteList()
    }
    
    func actionRefreshControlPulled() {
        fetchFavoriteList()
    }
    
    func actionDidSelectRowAtIndexPath(_ indexPath: IndexPath) {
        indexPathSelected = indexPath
        let stock = favoriteStocks[indexPath.row]
        self.delegate?.routeToStockDetailScreenWithModel(stock)
    }
    
    func actionDidSelectDeleteAtIndexPath(_ indexPath: IndexPath) {
        let stockToDelete = favoriteStocks[indexPath.row]
        repository?.deleteStockFromFavorite(stock: stockToDelete, completionBlock: { [weak self] (deleted: Bool) in
            self?.favoriteStocks.remove(at: indexPath.row)
            self?.delegate?.deleteRowAtIndexPath(indexPath)
        })
    }
    
    func fetchFavoriteList() {
        repository?.fetchFavoriteStocks({[weak self]( stocks: [Stock]) in
            self?.favoriteStocks = stocks
            DispatchQueue.main.async {
                self?.delegate?.endRefreshControl()
                self?.delegate?.reloadTable()
            }
        })
    }
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return favoriteStocks.count
    }
    
    func stockNameAtIndexPath(_ indexPath: IndexPath) -> String {
        let stock = favoriteStocks[indexPath.row]
        return stock.name  ?? ""
    }
    
    func isHideHotAtIndexPath(_ indexPath: IndexPath) -> Bool {
        let stock = favoriteStocks[indexPath.row]
        return (stock.hot != 1)
    }
    
    
}

