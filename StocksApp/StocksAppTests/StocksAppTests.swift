//
//  StocksAppTests.swift
//  StocksAppTests
//
//  Created by Carlos Jimenez on 19/6/21.
//

import XCTest

class StocksAppTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testFavoriteListViewModel() throws {
        let mockRepository = MockStockRepository()
        let viewModel = FavoriteListViewModel(repository: mockRepository, delegate: nil)
        viewModel.fetchFavoriteList()
        XCTAssert(viewModel.title == NSLocalizedString("Favorites", comment: ""))
        XCTAssert(viewModel.numberOfSections() == 1)
        XCTAssert(viewModel.numberOfRowsInSection(0) == 3)
        XCTAssert(viewModel.stockNameAtIndexPath(IndexPath(row: 0, section: 0)) == "Apple")
        XCTAssert(viewModel.isHideHotAtIndexPath(IndexPath(row: 0, section: 0)) == false)
        XCTAssert(viewModel.stockNameAtIndexPath(IndexPath(row: 1, section: 0)) == "Amazon")
        XCTAssert(viewModel.isHideHotAtIndexPath(IndexPath(row: 1, section: 0)) == false)
        XCTAssert(viewModel.stockNameAtIndexPath(IndexPath(row: 2, section: 0)) == "Tesla")
        XCTAssert(viewModel.isHideHotAtIndexPath(IndexPath(row: 2, section: 0)) == true)
        
        
        viewModel.actionDidSelectDeleteAtIndexPath(IndexPath(row: 0, section: 0))
        XCTAssert(viewModel.numberOfRowsInSection(0) == 2)
        XCTAssert(viewModel.stockNameAtIndexPath(IndexPath(row: 0, section: 0)) == "Amazon")
        XCTAssert(viewModel.isHideHotAtIndexPath(IndexPath(row: 0, section: 0)) == false)
        XCTAssert(viewModel.stockNameAtIndexPath(IndexPath(row: 1, section: 0)) == "Tesla")
        XCTAssert(viewModel.isHideHotAtIndexPath(IndexPath(row: 1, section: 0)) == true)
        
    }
    
    func testStockDetailViewModel() {
        let apple = Stock(id: "1", name: "Apple", hot: 1, ricCode: "ASLSLSLS", category: "NASDAQ")
        let viewModel = StockDetailViewModel(stock: apple, delegate: nil)
        XCTAssert(viewModel.title == NSLocalizedString("Detail", comment: ""))
        XCTAssert(viewModel.numberOfSections() == 1)
        XCTAssert(viewModel.numberOfRowsInSection(0) == 4)
        XCTAssert(viewModel.nameStockPropertyAtIndexPath(IndexPath(row: 0, section: 0)) == NSLocalizedString("name", comment: "").uppercased())
        XCTAssert(viewModel.nameStockPropertyAtIndexPath(IndexPath(row: 1, section: 0)) == NSLocalizedString("hot", comment: "").uppercased())
        XCTAssert(viewModel.nameStockPropertyAtIndexPath(IndexPath(row: 2, section: 0)) == NSLocalizedString("ricCode", comment: "").uppercased())
        XCTAssert(viewModel.nameStockPropertyAtIndexPath(IndexPath(row: 3, section: 0)) == NSLocalizedString("category", comment: "").uppercased())
        XCTAssert(viewModel.valueStockPropertyAtIndexPath(IndexPath(row: 0, section: 0)) == apple.name)
        XCTAssert(viewModel.valueStockPropertyAtIndexPath(IndexPath(row: 1, section: 0)) == "yes")
        XCTAssert(viewModel.valueStockPropertyAtIndexPath(IndexPath(row: 2, section: 0)) == apple.ricCode)
        XCTAssert(viewModel.valueStockPropertyAtIndexPath(IndexPath(row: 3, section: 0)) == apple.category)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
