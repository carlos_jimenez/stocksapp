//
//  MockStockRepository.swift
//  StocksApp
//
//  Created by Carlos Jimenez on 19/6/21.
//

import Foundation

class  MockStockRepository: StockRepository {
    
    init() {
        
    }
    
    func fetchFavoriteStocks(_ completionBlock: @escaping (_ favorites: [Stock]) -> Void) {
        let apple = Stock(id: "1", name: "Apple", hot: 1, ricCode: "ASLSLSLS", category: "NASDAQ")
        let amazon = Stock(id: "2", name: "Amazon", hot: 1, ricCode: "XLSLXLS", category: "NASDAQ")
        let tesla = Stock(id: "3", name: "Tesla", hot: 0, ricCode: "SJSJSJS", category: "NASDAQ")
        let stocks = [apple, amazon, tesla]
        completionBlock(stocks)
    }
    
    func deleteStockFromFavorite(stock: Stock, completionBlock: @escaping (_ success: Bool) -> Void) {
        completionBlock(true)
    }
}
